### Libretro Core Manager (lrcm) usage:

	 list                       List exact names of all official cores
	 show                       Show cores that are currently installed
	 add [CORE1] [CORE2]...     Add [CORE] to your retroarch folder
	 remove [CORE1] [CORE2]...  Remove [CORE] from your retroarch folder
	 update                     Update all of your libretro cores
	 log                        Output the contents of the logfile
	 
![screenshot](/uploads/003a4068df9a05ce95f66af799e624f0/screenshot.png)


### HOW TO INSTALL:

        git clone https://gitlab.com/toeshred/lrcm
        cd lrcm
        ./lrcm